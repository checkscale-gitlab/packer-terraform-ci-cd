variable "prefix_name" {}

variable "vpc_id" {}

variable "ssh_public_port" {
  default = 22
}

variable "ssh_port" {
  default = 22
}