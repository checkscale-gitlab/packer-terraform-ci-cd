variable "aws_region" {}

variable "prefix_name" {}

variable "vpc_cidr" {
  default = "10.1.0.0/16"
}

variable "public_subnet_cidr" {
  default = "10.1.1.0/24"
}

variable "azs" {
  type        = list
  description = "AZs to use in your public and private subnet  (make sure they are consistent with your AWS region)"
  default     = ["eu-west-3"]
}