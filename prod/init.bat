@echo off
Rem First Param is user, second param is password
Rem check
if "%1"=="" goto :error
if "%2"=="" goto :error
terraform init ^
-backend-config="address=https://gitlab.com/api/v4/projects/32093637/terraform/state/prod" ^
-backend-config="lock_address=https://gitlab.com/api/v4/projects/32093637/terraform/state/prod/lock" ^
-backend-config="unlock_address=https://gitlab.com/api/v4/projects/32093637/terraform/state/prod/lock" ^
-backend-config="username=%1" ^
-backend-config="password=%2" ^
-backend-config="lock_method=POST" ^
-backend-config="unlock_method=DELETE" ^
-backend-config="retry_wait_min=5"
goto :done

:error
echo "Two param, first username, second, password"

:done
