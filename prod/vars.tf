variable "AWS_ACCESS_KEY_ID" {
  type = string
}
variable "AWS_SECRET_ACCESS_KEY" {
  type = string
}
variable "AWS_REGION" {
  default = "eu-west-3"
  description = "Région AWS"
}

variable "prefix_name" {
  default = "packer-test"
}