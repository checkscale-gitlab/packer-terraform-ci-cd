provider "aws" {
  region     = var.AWS_REGION
  access_key = var.AWS_ACCESS_KEY_ID
  secret_key = var.AWS_SECRET_ACCESS_KEY
}

module "vpc" {
  source = "./modules/vpc"
  prefix_name = var.prefix_name
  aws_region = var.AWS_REGION
}

module "asg" {
  source = "./modules/asg"
  prefix_name = var.prefix_name
  vpc_id = module.vpc.vpc_id
}

data "template_file" "init" {
  template = file("../aws-ubuntu.pkr.hcl")
  vars ={
    vpc_id = module.vpc.vpc_id
    subnet_id = module.vpc.public_subnet_id
  }
}

resource "local_file" "packer-file" {
  content = data.template_file.init.rendered
  filename = "../aws-ubuntu-provided.pkr.hcl"
}