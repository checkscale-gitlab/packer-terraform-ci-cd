resource "aws_security_group" "public_sg" {
  vpc_id      = var.vpc_id
  name        = "${var.prefix_name}-sg-webserver"
  description = "Security group for webserver instances"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = var.ssh_public_port
    to_port     = var.ssh_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  tags = {
    Name = "${var.prefix_name}-sg-instance"
  }
}